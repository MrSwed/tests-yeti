jQuery(document).ready(function ($) {

  $(document).on("click", ".js-getRoutePrice", function (e) {
    e.preventDefault();
    if (!$("body #modal-1").length) {
      $.get("modal/modal1.html", function (data) {
        $("body").append(data);
        // $(this).attr("data-micromodal-trigger", "modal-1");
        MicroModal.init();
        MicroModal.show('modal-1')
      });
    } else {
      MicroModal.init();
      MicroModal.show('modal-1')
    }
  })
    .on("submit", ".js-getGuide", function (e) {
      e.preventDefault();
      MicroModal.close('modal-1')

      if (!$("body #modal-2").length) {
        $.get("modal/modal2.html", function (data) {
          $("body").append(data);
          // $(this).attr("data-micromodal-trigger", "modal-2");
          MicroModal.init();
          MicroModal.show('modal-2')
        });
      } else {
        MicroModal.init();
        MicroModal.show('modal-2')
      }
    });
});